/**
 * Helpers
 */
function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n)
}

/**
 * Reverse string without .reverse() method
 * @param str
 */
function reverseString(str){
    if (!str || str.length === 0) return console.log('There is no string inside');
    let letterArr = str.split('');
    let result = '';

    for (let i=1; i <= letterArr.length; i++) {
        result = result + letterArr[letterArr.length - i];
    }

    return console.log(`String - ${str} \nReverse method result is - ${result}`);
}

// reverseString('qwerty');

/**
 * String word counter
 * @param str
 */
function wordCounter(str){
    if (!str || str.length === 0) return console.log('There is no string inside');
    let wordArr = str.split(' ');
    let result = {};

    for (let i=0; i < wordArr.length; i++) {
        result[wordArr[i].toLowerCase()] = result[wordArr[i].toLowerCase()] ? result[wordArr[i].toLowerCase()] + 1 : 1;
    }

    return console.log(`String - ${str}, \nWord counter result is`, result);
}

// wordCounter('Hello world hello kitty hello');

/**
 * String word counter
 * @param obj - default object
 * @param keys - property to exclude from default object
 */
function objectExcluder(obj, ...keys){
    let result = {...obj};

    for (let key in result) {
        for (let i=0; i < keys.length; i++) {
            if (key === keys[i]) {
                delete result[key];
            }
        }
    }

    return console.log('Default obj - ', obj, '\nKeys to exclude - ', keys,'\nResult obj - ', result);
}

// objectExcluder({ a: 1, b: 2, c: 3, d: 4 }, 'a', 'd');

/**
 * Объекты: перебор свойств (https://learn.javascript.ru/)
 */

/**
 * Умножьте численные свойства на n
 */
function multiplyNumeric(obj, n = 2) {
    if (!obj) return console.log('There is no obj inside');
    let res = {...obj};

    for (let key in res) {
        let item = res[key];
        console.log(isNumeric(item));
        if (isNumeric(res[key])) {
            console.log(res[key]);
            res[key] *= n;
        }
    }
    return console.log('Default obj - ', obj, '\nN - ', n, '\nResult obj - ', res);
}

// let menu = {
//     width: 200,
//     height: 300,
//     title: "My menu"
// };

// multiplyNumeric(menu, 3);

/**
 * Определите, пуст ли объект
 */
function isEmpty(obj) {
    let res = true;

    if (Object.keys(obj).length > 0) {
        res = false;
    }

    return console.log('Default obj - ', obj, '\nIs empty - ', res);
}

// let schedule = {};
//
// schedule["8:30"] = "подъём";
//
// isEmpty(schedule); // true

/**
 * Cyмма свойств
 */
function salaryCalc(obj) {
    if (!obj) return console.log('There is no obj inside');
    let count = 0;
    for (let key in obj) {
        count += obj[key];
    }

    return console.log(obj, 'Salaries count - ' + count);
}

// let salaries = {
//     "Вася": 100,
//     "Петя": 300,
//     "Даша": 250
// };
//
// salaryCalc(salaries);

/**
 * Свойство с наибольшим значением
 */
function salaryMax(obj) {
    if (!obj) return console.log('There is no obj inside');

    let maxSalary = 0;
    let maxName = '';

    for (let key in obj) {
        if (obj[key] > maxSalary) {
            maxSalary = obj[key];
            maxName = key;
        }
    }

    return console.log(obj, 'Max salary - ' + maxSalary + ' у ' + maxName);
}

// let salaries = {
//     "Вася": 100,
//     "Петя": 300,
//     "Даша": 250
// };
//
// salaryMax(salaries);

/**
 * Codewars
 */

/**
 * An isogram is a word that has no repeating letters, consecutive or non-consecutive.
 * Implement a function that determines whether a string that contains only letters is an isogram.
 * Assume the empty string is an isogram.
 * Ignore letter case.
 * isIsogram( "Dermatoglyphics" ) == true
 * isIsogram( "aba" ) == false
 * isIsogram( "moOse" ) == false // -- ignore letter case
 * @param str
 * @returns {boolean}
 */
function isIsogram(str){
    if (!str || str.length === 0) return true;

    let strArr = str.toLowerCase().split('');
    let obj = {};
    let res = 0;

    for (let i = 0; i < strArr.length; i++) {
        obj[strArr[i]] = ~~obj[strArr[i]] + 1;
    }

    for (let key in obj) {
        if (obj.hasOwnProperty(key) && obj[key] > 1) {
            res += obj[key];
        }
    }
    console.log(`${(res === 0) ? `There is no duplicate letters\n ${str}` : `Here we have some duplicates :(\n ${str}`}`);
    return res === 0;
}

// isIsogram("Dermatoglyphics");
// isIsogram("aba");
// isIsogram("moOse");

/**
 * Given two integers a and b, which can be positive or negative,
 * find the sum of all the numbers between including them too and return it.
 * If the two numbers are equal return a or b.
 * Note: a and b are not ordered!
 * GetSum(1, 0) == 1   // 1 + 0 = 1
 * GetSum(1, 2) == 3   // 1 + 2 = 3
 * GetSum(0, 1) == 1   // 0 + 1 = 1
 * GetSum(1, 1) == 1   // 1 Since both are same
 * GetSum(-1, 0) == -1 // -1 + 0 = -1
 * GetSum(-1, 2) == 2  // -1 + 0 + 1 + 2 = 2
 */
function GetSum(a, b) {
    if (a === b) return a;
    let sum = 0;
    if (b > a) {
        for (a; a < b + 1; a++) {
            sum += a;
        }
        return sum;
    } else {
        for (b; b < a + 1; b++) {
            sum += b;
        }
        return sum;
    }
}

// console.log(GetSum(1, 10));
// console.log(GetSum(0, -1));
// console.log(GetSum(0, 1));
// console.log(GetSum(1, 1));

function friend(friends){
    let arr = friends;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i].length !== 4) {
            arr.splice(arr.indexOf(arr[i]), 1);
            i--;
        }
    }
    return console.log(arr);
}

// friend(["Ryan", "Kieran", "Mark"]);
// friend(["Ryan", "Jimmy", "123", "4", "Cool Man"]);
// friend(["Jimm", "Cari", "aret", "truehdnviegkwgvke", "sixtyiscooooool"]);
// friend(["Love", "Your", "Face", "1"]);

// '[0, 0, 0, 1] => 1'

const binaryArrayToNumber = arr => {
    return parseInt(arr.join(''), 2);
};

// binaryArrayToNumber([0, 0, 0, 1]);

function disemvowel(str) {
    let vowels = ['a', 'A', 'e', 'E', 'i', 'I', 'o', 'O'];
    let res = str;
    let reg;
    for (let i = 0; i < vowels.length; i++) {
        reg = new RegExp(vowels[i], 'g');
        res = res.replace(reg, '');
    }
    return console.log(str, res);
}

// disemvowel("This website is for losers LOL!");

function towerBuilder(nFloors) {
    let arr = [];
    for (let i = nFloors; i > 0; i--) {
        arr[i - 1] = arr[i] ? arr[i].replace('*', '***') : '*';
    }
    let max = arr[0].length;

    for (let i = 1; i < nFloors; i++) {
        do {
            arr[i] = ` ${arr[i]} `;
        } while (arr[i].length < max)
    }
    return arr.reverse();
}

// console.log(towerBuilder(5));
// console.log(towerBuilder(1));
// console.log(towerBuilder(15));
// console.log(towerBuilder(65));

function Fighter(name, health, damagePerAttack) {
    this.name = name;
    this.health = health;
    this.damagePerAttack = damagePerAttack;
    this.toString = function() { return this.name; }
}

function declareWinner(fighter1, fighter2, firstAttacker) {
    let first = fighter1.name !== firstAttacker ? fighter1 : fighter2;
    let second = fighter1.name === firstAttacker ? fighter1 : fighter2;
    let winner = '';
    do {
        if (first.health > 0) {
            first.health -= second.damagePerAttack;
            if (first.health <= 0) {
                return winner = second.name;
            }
        }
        if (second.health > 0) {
            second.health -= first.damagePerAttack;
            if (second.health <= 0) {
                return winner = first.name;
            }
        }
    } while (first.health > 0 || second.health > 0);
}

// console.log(declareWinner(new Fighter("Lew", 10, 2), new Fighter("Harry", 5, 4), "Lew"));
// console.log(declareWinner(new Fighter("Lew", 10, 2), new Fighter("Harry", 5, 4), "Harry"));

function rowSumOddNumbers(n) {
    let arr = new Array(n).fill(0);
    let pow = n % 2 ? Math.pow(n, 2) : Math.pow(n, 2) - 1;
    let k = n > 3 && !(n % 2) ? 1 : 0;
    let posCount = n % 2 ? (n - 1) / 2 : n / 2 - k ;
    let delta = n > 2 ? pow - 2 * posCount : pow;
    console.log('pow ' + pow, 'posCount ' + posCount, 'delta ' + delta);
    arr.forEach((_, i, arr) => arr[i] = delta + i * 2 );
    console.log(arr);
    return arr.reduce((a, b) => a + b, 0);
}

//Dolboeb bleat!!
// function rowSumOddNumbers(n) {
//     return Math.pow(n, 3);
// }
// rowSumOddNumbers(1);
// rowSumOddNumbers(2);
// rowSumOddNumbers(3);
// rowSumOddNumbers(4);
// rowSumOddNumbers(5);
// rowSumOddNumbers(6);
// rowSumOddNumbers(7);
// rowSumOddNumbers(8);
// rowSumOddNumbers(21);

let SequenceSum = (function() {
    function SequenceSum() {}

    SequenceSum.showSequence = function(count) {
        if (count === 0) return '0=0';
        if (count < 0) return `${count}<0`;
        let arr = [];
        for (let i=1; i < count + 1; i++) {
            arr[i - 1] = i;
        }
        return `0+${arr.join('+')} = ${arr.reduce((a, b) => a + b)}`;
    };

    return SequenceSum;

})();
console.log(SequenceSum.showSequence(-15));
console.log(SequenceSum.showSequence(0));
console.log(SequenceSum.showSequence(5));